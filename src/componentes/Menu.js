import React from 'react';
import {Link} from 'react-router-dom';

const Menu = props => {
    return (            
        <div className='menu main-menu'>
            <nav className='container navbar navbar-expand-md'>
                <button className='navbar-dark navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExampleDefault' aria-controls='navbarsExampleDefault' aria-expanded='false' aria-label='Toggle navigation'><span className='navbar-toggler-icon'></span></button>
                <div className='collapse navbar-collapse' id='navbarsExampleDefault'>
                    <ul className='navbar-nav mr-auto'>
                        <li className='nav-item'><Link className='nav-link' to='/'>Início</Link></li>                       
                        <li className='nav-item dropdown'>
                            <Link className='nav-link dropdown-toggle' to='' id='dropdown01' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Soluções</Link>
                            <div className='dropdown-menu' aria-labelledby='dropdown01'>
                                <Link className='dropdown-item' to='/nivelamento'>Nivelamento</Link>
                                <Link className='dropdown-item' to='/enade'>ENADE</Link>
                                <Link className='dropdown-item' to='/aprendizagem'>Saraiva solução de aprendizagem</Link>
                                <Link className='dropdown-item' to='/biblioteca'>Biblioteca digital</Link>
                                <Link className='dropdown-item' to='/aprova'>Saraiva aprova</Link>
                            </div>
                        </li>
                        <li className='nav-item'><Link className='nav-link' to='/blog'>Blog</Link></li>                        
                    </ul>
                </div>
            </nav>
        </div>
    )
}
export default Menu;