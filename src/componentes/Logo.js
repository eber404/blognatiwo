import React from 'react';
import {Link} from 'react-router-dom';

const Logo = props => {
    return (
        <header className='container'>
            <Link className='nav-link' to='/'><img src='img/logo-saraiva-educacao.svg' alt='Logo Saraiva Educação'/></Link>		
            <Link className='btn-secondary' to='/contato'><img src='icons/contato.svg' width='21' height='21' alt='Contato'/>Fale com a gente</Link>
        </header>
    )
}
export default Logo;