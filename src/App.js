import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import Inicio from './paginas/Inicio';
import Logo from './componentes/Logo';
import Menu from './componentes/Menu';
import Rodape from './componentes/Rodape';
import Blog from './paginas/Blog';
import Post from './paginas/Post';
import Contato from './paginas/Contato';
import Nivelamento from './paginas/solucoes/Nivelamento';
import Enade from './paginas/solucoes/Enade';
import Aprendizagem from './paginas/solucoes/Aprendizagem';
import Biblioteca from './paginas/solucoes/Biblioteca';
import Aprova from './paginas/solucoes/Aprova';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
        <Logo />
        <Menu />
        {/*CONTEUDO*/}
        
        <Route path='/' exact component = {Inicio} />
        <Route path='/blog' component = {Blog} />
        <Route path='/contato' component = {Contato} />
        <Route path='/nivelamento' component = {Nivelamento} />
        <Route path='/enade' component = {Enade} />
        <Route path='/aprendizagem' component = {Aprendizagem} />
        <Route path='/biblioteca' component = {Biblioteca} />
        <Route path='/aprova' component = {Aprova} />
        <Route path='/post' component = {Post} />

        {/*========*/}
        <Rodape />
      </div>
      </BrowserRouter>
    );
  }
} 

export default App;