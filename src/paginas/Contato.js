import React from 'react';

const Contato = props => {
    return (
        <main>
            <section className='container contato'>
            	<div className="row">
            		<div className="col-md-7">
            			<img src='img/sobre.png' alt='Sobre nós'/>            			
            		</div>
            		<div className="col-md-5 formulario">
        				<form name="fale-com-especialista" id="fale-com-especialista" autocomplete="off">
        					<h1>Fale com um de nossos especialistas</h1>
            				<p>Entraremos em contato por telefone ou email.</p>
        					<div className="form-group"><input type="text" className="form-control" id="inputName" placeholder="Nome" required="" name="nome" /></div>
        					<div className="form-group"><input type="email" className="form-control" id="inputEmail" placeholder="Email" required="" name="email" /></div>
        					<div className="form-group"><input className="form-control" id="inputPhone" placeholder="Telefone" required="" name="telefone" /></div>
        					<div className="form-group"><input type="text" className="form-control" id="inputIES" placeholder="Nome da IES" required="" name="ies" /></div>
        					<div className="form-group">
        						<select className="form-control" id="inputCargo" required="" name="cargo">
            						<option>Cargo na IES</option>
            						<option value="Diretor(a)">Diretor(a)</option>
            						<option value="Coordenador(a)">Coordenador(a)</option>
            						<option value="Professor(a)">Professor(a)</option>
            						<option value="Aluno(a)">Aluno(a)</option>
            						<option value="Outro">Outro</option>
        						</select>
        					</div>
        					<button type="submit" className="btn btn-primary btn">Enviar</button>
        				</form> 
            		</div>           		            		
            	</div>
            </section>
        </main>        
    )
}
export default Contato;