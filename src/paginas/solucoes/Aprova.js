import React from 'react';
import {Link} from 'react-router-dom';

const Aprova = props => {
    return (
        <main>
            {/* SARAIVA APROVA */}
            <section className='container'>
            	<div className='row solucao-img'>
            		{/* <img src='img/nivelamento.svg' alt='Logo Saraiva Educação | Nivelamento'/> */}
            	</div>
            	<div className='row'>
           		
            		{/* MENU LATERAL ESQUERDO */}
            		<nav className='col-md-3'>
            			<div className='list-group navegacao'>
							<Link to='./#nivelamento' className='list-group-item list-group-item-action'>O Saraiva Aprova</Link>
							<Link to='./#beneficios' className='list-group-item list-group-item-action'>Benefícios Gerais</Link>
							<Link to='./#funcionalidades' className='list-group-item list-group-item-action'>Funcionalidades</Link>
							<Link to='./#depoimentos' className='list-group-item list-group-item-action'>Depoimentos</Link>
						</div>	
            		</nav>

            		
            		{/* CONTEUDO */}
            		<div className='col-md-9'>
            			<h3 className='subtitulos' id='nivelamento'>O Saraiva Aprova promove:</h3>
            			<div className='row icones'>
            				<div className='col-md-4'>
            					<img src='icons/trilhas.svg' alt=''/>
            					<p>Engajamento dos alunos</p>
            				</div>
            				<div className='col-md-4'>
            					<img src='icons/relatorios.svg' alt=''/>
            					<p>Acompanhamento de performance</p>
            				</div> 
            				<div className='col-md-4'>
            					<img src='icons/indicadores.svg' alt=''/>
            					<p>Melhor desempenho no exame da OAB</p>
            				</div>    				
            			</div>

						{/* TEXTO SOLUCAO */}
            			<div className='row texto'>
            				<p>O Saraiva Aprova é uma solução capaz de aumentar o desempenho dos alunos do curso de Direito no Exame oficial do Conselho Federal da Ordem dos Advogados do Brasil (OAB) por meio de trilhas adaptativas com conteúdo multiformato e relatórios de desempenho por disciplina. Nós, da Saraiva Educação, temos compromisso com o aprendizado e confiança na qualidade do nosso conteúdo.</p>
            			</div>
						
						{/* BENEFÍCIOS */}
            			<div className='row' id='beneficios'>
            				<h3 className='subtitulos'>Benefícios Gerais</h3>
            				<ul className='lista'>
            					<li>São 220 horas de videoaulas com temáticas atualizadas, linguagem clara e objetiva, recursos gráficos e conteúdos interdisciplinares;</li>
            					<li>Mais de 20 mil questões disponíveis em uma plataforma com inteligência estatística para garantir que o aluno seja exposto aos temas mais recorrentes no Exame da OAB;</li>
            					<li>Plataforma fácil e intuitiva;</li>
            					<li>As aulas ficam disponíveis na plataforma desde o primeiro acesso do aluno. Assim, ele poderá assistir sempre e quantas vezes quiser;</li>
            					<li>Vade Mecum Saraiva OAB e Concursos, com todos os seus artigos e notas incorporados nas respostas das questões. Cronograma detalhado, material de estudo completo, resumos, esquemas e infográficos para tornar o estudo mais dinâmico.</li>
            				</ul>
            			</div>

            			{/* FUNCIONALIDADES */}
            			<div className='row' id='funcionalidades'>
            				<h3 className='subtitulos'>Principais Funcionalidades</h3>
            				<ul className='lista'>
            					<li>Trilhas de aprendizagem: organizamos as disciplinas em módulos temáticos</li>
            					<li>Videoaulas de até 20 minutos para garantir os melhores níveis de aprendizagem. Assim, o aluno pode assistir de onde estiver através de qualquer dispositivo móvel;</li>
            					<li>Relatório para acompanhamento de desempenho por disciplina para que o aluno saiba quais são as disciplinas que precisarão ser reforçadas até o dia da prova.</li>
            				</ul>
            			</div>

            			{/* DEPOIMENTOS */}
            			<div className='row' id='depoimentos'>
            				<h3 className='subtitulos'>Depoimentos</h3>
            				<ul className='lista'>
            					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis euismod risus.</li>
            					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis euismod risus.</li>
            				</ul>
            			</div>

            			{/* CALL TO ACTION */}
            			<div className='row call'>
            				<div className="col-md-7">
            					<h4 className='subtitulos'>Quer aumentar o desempenho de seus alunos no Exame da OAB?</h4>
            				</div>
            				<div className="col-md-5">
            					<Link className='btn botao' to='/contato'>Fale com um especialista</Link>            					
            				</div>
            			</div>						
            		</div>
            	</div>               
            </section>
        </main>         
    )
}
export default Aprova;