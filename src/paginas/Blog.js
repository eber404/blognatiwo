import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import blogApi from '../services/blogApi';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
const blog = new blogApi();

export default class Blog extends Component {
	constructor(props) {
		super(props);

		this.pageNumber = 1;
		this.pages = 0;

		this.state = {
			posts: [],
			totalPosts: 0,
			lastPostContent: '',
			lastAuthorName: '',
			lastPostTitle: '',
			lastPostDate: '',
			lastPostImg: '',
			lastPostId: '',
			lastPost: '',
			current: 1,
		};
	}

	async loadLastPost() {
		const lastPost = await blog.load(`/posts/?_embed&per_page=1`);
		const author = await blog.load(`/users/${lastPost[0].author}`);

		this.setState({
			lastPostContent: lastPost[0].excerpt.rendered,
			lastAuthorName: author.name,
			lastPostTitle: lastPost[0].title.rendered,
			lastPostDate: lastPost[0].date_gmt,
			lastPostImg: lastPost[0]._embedded['wp:featuredmedia']['0'].source_url,
			lastPostId: lastPost[0].id,
			lastPost: lastPost[0],
		});
	}

	async loadPosts() {
		const totalPosts = await blog.getHeader(`/posts/?_embed&per_page=1`);
		const posts = await blog.load(`/posts/?_embed&per_page=7&page=${this.state.current}`);

		this.setState({
			posts,
			totalPosts: parseInt(totalPosts.headers['x-wp-totalpages']),
		});
	}

	onChange = page => {
		console.log(page);
		this.setState({
			current: page,
		});
		this.loadPosts();
	};

	componentDidMount() {
		this.loadLastPost();
		this.loadPosts();
	}

	render() {
		const {
			posts,
			lastAuthorName,
			lastPostTitle,
			lastPostDate,
			lastPostImg,
			lastPostId,
			lastPost,
			lastPostContent,
			totalPosts,
		} = this.state;

		return (
			<main>
				{/* POST DESTAQUE (MAIS RECENTE) */}

				<section className="container esp-top">
					<img className="sombra-post" src="img/row-top.jpg" alt="Linha" />
					<article className="row">
						<div className="col-md-7 post-principal">
							<img src={lastPostImg} alt="Post principal" />
						</div>
						<div className="col-md-5">
							<article className="post-principal-content">
								<h1>{lastPostTitle}</h1>
								<p className="autor-blog">
									Por <strong>{lastAuthorName}</strong> | {moment(lastPostDate).format('LL')}{' '}
								</p>
								<p dangerouslySetInnerHTML={{ __html: lastPostContent }} />
								<Link
									to={`/post?id=` + lastPostId}
									onClick={() => {
										/* window.scrollTo(0, 220) */
										sessionStorage.clear();
										sessionStorage.post = JSON.stringify(lastPost);
									}}
								>
									Continuar lendo
								</Link>
							</article>
						</div>
					</article>
					<img className="sombra-post" src="img/row-bot.jpg" alt="Linha" />
				</section>

				{/* BLOG POSTS */}
				<section className="esp-top solucoes container">
					<div className="row">
						{posts.map((post, index) =>
							index > 0 ? (
								<article className="post col-md-4" key={post.id}>
									<img
										src={post._embedded['wp:featuredmedia']['0'].source_url}
										alt="Imagem do post"
									/>
									<div className="content gray">
										<h3>{post.title.rendered}</h3>
										<p dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }} />
										<Link
											to={`/post?id=` + post.id}
											onClick={() => {
												sessionStorage.clear();
												sessionStorage.post = JSON.stringify(post);
											}}
										>
											Continuar lendo
										</Link>
									</div>
								</article>
							) : (
								''
							)
						)}
					</div>

					{/* PAGINACAO (POSTS) */}
					<nav className="paginacao">
						<ul className="pagination justify-content-center">
							<Pagination onChange={this.onChange} current={this.state.current} total={totalPosts} />
						</ul>
					</nav>
				</section>
			</main>
		);
	}
}
