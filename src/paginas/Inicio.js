import React from 'react';
import {Link} from 'react-router-dom';

const Inicio = props => {
    return (
        <main>
            {/* BANNER CALL TO ACTION */}
            <section className='banner container'>
                <img src='img/banner.jpg' alt='Saraiva Educação'/>
                <div className='content'>
                    <h1>Quer melhorar o desempenho da sua IES?</h1>
                    <p>Conheça as soluções da Saraiva Educação e saiba como cada uma delas pode ser útil para sua instituição.</p>
                    <Link className='btn btn-primary btn-lg' to='/contato'>Fale com a gente</Link>
                </div>
            </section>

            {/* SOBRE NÓS */}
            <section className='sobre container'>
                <div className='row'>
                    <div className='col-md-5'>
                        <h2>Sobre nós</h2>
                        <p>Referência nos mercados editorial e educacional brasileiros há décadas, a Saraiva Educação oferece hoje muito mais do que livros: nosso propósito é transformar a educação no Brasil. Queremos contribuir com o crescimento da sua IES e por isso, estamos constantemente desenvolvendo soluções inteligentes que acompanham as tendências e mudanças do setor. Nossos produtos são totalmente orientados para que as instituições de ensino superior atendam a importantes requisitos regulatórios, além do foco em facilitar tanto o trabalho do professor, quanto a aprendizagem do aluno. Como podemos ajudar a sua IES?</p>
                    </div>
                    <div className='col-md-7'>
                        <img src='img/sobre.png' alt='Sobre nós'/>
                    </div>
                </div>
                <div className='row sombra-bot'>
                    <img src='img/row-bot.jpg' alt='Linha' />
                </div>
            </section>

	        {/* SOLUÇÕES */}
	        <section className='solucoes container'>
	        	<h1>Nossas Soluções</h1>
                <div className='row'>
                    <article className='col-md-6 col-sm-12 solution'>
                        <img src='/img/nivelamento.jpg' alt='Nivelamento' />
                        <div className='content'>
                            <h3>Nivelamento</h3>
                            <p>Saiba como lidar com o desafio da defasagem de conhecimento entre os alunos ingressantes e aumentar sua captação.</p>
                        </div>
                        <div className='link'>
                            <Link to='/nivelamento'>Saiba mais &gt;</Link>
                        </div>
                    </article>
                    <article className='col-md-6 col-sm-12 solution'>
                        <img src='/img/enade.jpg' alt='Enade' />
                        <div className='content'>
                            <h3>ENADE</h3>
                            <p>Garanta à sua Instituição de Educação Superior melhores resultados no Enade.</p>
                        </div>
                        <div className='link'>
                            <Link to='/enade'>Saiba mais &gt;</Link>
                        </div>
                    </article>
                </div>
                <div className='row'>
                <article className='col-md-6 col-sm-12 solution'>
                        <img src='/img/ssa.jpg' alt='Aprendizagem' />
                        <div className='content'>
                            <h3>Aprendizagem</h3>
                            <p>Ofereça metodologias ativas de aprendizagem e desenvolva competências e habilidades previstas nas DCNs de cada curso.</p>
                        </div>
                        <div className='link'>
                            <Link to='/aprendizagem'>Saiba mais &gt;</Link>
                        </div>
                    </article>
                    <article className='col-md-6 col-sm-12 solution'>
                        <img src='/img/biblioteca-digital.jpg' alt='Biblioteca' />
                        <div className='content'>
                            <h3>Biblioteca Digital</h3>
                            <p>Tenha fácil acesso a títulos atualizados de autores renomados com a qualidade Saraiva.</p>
                        </div>
                        <div className='link'>
                            <Link to='/biblioteca'>Saiba mais &gt;</Link>
                        </div>
                    </article>
                </div>
                <div className='row'>
                <article className='col-md-6 col-sm-12 solution'>
                        <img src='/img/aprova.jpg' alt='Saraiva Aprova' />
                        <div className='content'>
                            <h3>Saraiva Aprova</h3>
                            <p>Melhore o desempenho de seus estudantes de Direito e aumente o índice de aprovação da sua IES no Exame da OAB.</p>
                        </div>
                        <div className='link'>
                            <Link to='/aprova'>Saiba mais &gt;</Link>
                        </div>
                    </article>
                    <article className='col-md-6 col-sm-12 solution'>
                        <img src='/img/blog.png' alt='Blog' />
                        <div className='content'>
                            <h3>Blog Saraiva Educação</h3>
                            <p>Tenha comodidade e facilidade para manter o acervo físico de sua biblioteca sempre atualizado.</p>
                        </div>
                        <div className='link'>
                            <Link to='/blog'>Saiba mais &gt;</Link>
                        </div>
                    </article>
                </div>
            </section>
        </main>        
    )
}
export default Inicio;