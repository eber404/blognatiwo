import axios from 'axios';

const blog = axios.create({
	baseURL: 'https://blog.saraivaeducacao.com.br/wp-json/wp/v2/',
});

export default class blogApi {
	//carrega os informações do blog wordpress
	load = async (args = '') => {
		try {
			const response = await blog.get(`${args}`);
			return response.data;
		} catch (error) {}
	};

	getHeader = async (args = '') => {
		try {
			const response = await blog.get(`${args}`);
			return response;
		} catch (error) {}
	};
}
